package preprocessing;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



import java.text.ParseException;

public class Processor {

	static List<List<String>> records = new ArrayList<>();
	private static ArrayList<String> attributsListe = new ArrayList<String>();

	private static List<Integer> tempListe = new ArrayList<Integer>();

	private static ArrayList<Integer> ageListe = new ArrayList<Integer>();
	private static ArrayList<Integer> educationListe = new ArrayList<Integer>();
	private static ArrayList<String> workclassListe = new ArrayList<String>();
	private static ArrayList<String> marital_statusListe = new ArrayList<String>();
	private static ArrayList<String> occupationListe = new ArrayList<String>();
	private static ArrayList<String> relationshipListe = new ArrayList<String>();
	private static ArrayList<String> raceListe = new ArrayList<String>();
	private static ArrayList<String> genderListe = new ArrayList<String>();
	private static ArrayList<Integer> capital_gainsListe = new ArrayList<Integer>();
	private static ArrayList<Integer> capital_lossListe = new ArrayList<Integer>();
	private static ArrayList<Integer> hoursListe = new ArrayList<Integer>();
	private static ArrayList<String> countryListe = new ArrayList<String>();
	private static ArrayList<Integer> incomeListe = new ArrayList<Integer>();

	private static ArrayList<Double> fageListe = new ArrayList<Double>();
	private static ArrayList<Double> feducationListe = new ArrayList<Double>();
	private static ArrayList<Double> fworkclassListe = new ArrayList<Double>();
	private static ArrayList<Double> fmarital_statusListe = new ArrayList<Double>();
	private static ArrayList<Double> foccupationListe = new ArrayList<Double>();
	private static ArrayList<Double> frelationshipListe = new ArrayList<Double>();
	private static ArrayList<Double> fraceListe = new ArrayList<Double>();
	private static ArrayList<Double> fgenderListe = new ArrayList<Double>();
	private static ArrayList<Double> fcapital_gainsListe = new ArrayList<Double>();
	private static ArrayList<Double> fcapital_lossListe = new ArrayList<Double>();
	private static ArrayList<Double> fhoursListe = new ArrayList<Double>();
	private static ArrayList<Double> fcountryListe = new ArrayList<Double>();
	private static ArrayList<Double> fincomeListe = new ArrayList<Double>();

	private static ArrayList<String> fworkclassAttributsListe = new ArrayList<String>();
	private static ArrayList<String> fmartial_statusAttributsListe = new ArrayList<String>();
	private static ArrayList<String> foccupationAttributsListe = new ArrayList<String>();
	private static ArrayList<String> frelationshipAttributsListe = new ArrayList<String>();
	private static ArrayList<String> fraceAttributsListe = new ArrayList<String>();
	private static ArrayList<String> fcountryAttributsListe = new ArrayList<String>();
	static List<List<Integer>> workclassValueListe = new ArrayList<>();

	public static void main(String[] args) {
		// To-Do:
		// int verwandlung: gender,
		// teilen: age, education_num , capital gain , capital loss
		// one hot encoding: workclass, marital-status, occupation, relationship, race,
		// native-country
		try {
			eingabe();
			ausgabe();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void eingabe() throws IOException {
		/*
		 * String s1 = "test.csv";
		 * Files.walk(Paths.get("input")).filter(Files::isRegularFile) .forEach( x -> s1
		 * = "test.csv"); System.out.println(s1);
		 */
		BufferedReader br2 = new BufferedReader(new FileReader("input/" + "train.csv"));

		String line;
		while ((line = br2.readLine()) != null) {
			// String[] values = line.split(COMMA_DELIMITER);
			// System.out.println(line);
			String[] values = line.split(";");
			records.add(Arrays.asList(values));

		}

		for (int i = 1; i < records.size(); i++) {
			// for (int k = 0; k < records.get(i).size(); k++) {
			ageListe.add(Integer.valueOf(records.get(i).get(0)));
			workclassListe.add(records.get(i).get(1));
			educationListe.add(Integer.valueOf(records.get(i).get(2)));
			marital_statusListe.add(records.get(i).get(3));
			occupationListe.add(records.get(i).get(4));
			relationshipListe.add(records.get(i).get(5));
			raceListe.add(records.get(i).get(6));
			genderListe.add(records.get(i).get(7));
			capital_gainsListe.add(Integer.valueOf(records.get(i).get(8)));
			capital_lossListe.add(Integer.valueOf(records.get(i).get(9)));
			hoursListe.add(Integer.valueOf(records.get(i).get(10)));
			countryListe.add(records.get(i).get(11));
			incomeListe.add(Integer.valueOf(records.get(i).get(12)));

		}
		// Attributsliste fuellen
		attributsListe.add("gender");
		attributsListe.add("age");
		attributsListe.add("education");
		attributsListe.add("capital_gains");
		attributsListe.add("capital_loss");
		
		// int verwandlung: gender
		for (int i = 0; i < genderListe.size(); i++) {
			if (genderListe.get(i).contains("Male")) {
				fgenderListe.add(1d);
			} else if (genderListe.get(i).contains("Female")) {
				fgenderListe.add(0d);
			}
		}
		// teilen(Max wert): age, education_num , capital gain , capital loss
		for (int i = 0; i < ageListe.size(); i++) {
			double d = (double) ageListe.get(i) / 90;
			fageListe.add(d);
		}
		for (int i = 0; i < educationListe.size(); i++) {
			double d = (double) educationListe.get(i) / 16;
			feducationListe.add(d);
		}
		for (int i = 0; i < capital_gainsListe.size(); i++) {
			double d = (double) capital_gainsListe.get(i) / 99999;
			fcapital_gainsListe.add(d);
		}
		for (int i = 0; i < capital_lossListe.size(); i++) {
			double d = (double) capital_lossListe.get(i) / 4356;
			fcapital_lossListe.add(d);
		}
		// one hot encoding: workclass, marital-status, occupation, relationship, race,
		// country
		// List<List<Integer>> workclassValueListe = new ArrayList<>();
		// workclass
		// ------------------------------------------------------------------------------------------------------
		ArrayList<String> workclassAttributsListe = (ArrayList<String>) workclassListe.clone();

		for (int i = 0; i < workclassAttributsListe.size(); i++) {
			if (workclassAttributsListe.get(i) != "done") {
				for (int j = 1; j < workclassAttributsListe.size(); j++) {
					if (workclassAttributsListe.get(i).equals(workclassAttributsListe.get(j)) && i != j) {
						workclassAttributsListe.set(j, "done");
					}
				}
			}
		}
		for (int i = 0; i < workclassAttributsListe.size(); i++) {
			if (workclassAttributsListe.get(i) != "done") {
				fworkclassAttributsListe.add(workclassAttributsListe.get(i));
				attributsListe.add("workclass_" + workclassAttributsListe.get(i));
			}
		}

		// martial-status
		// ------------------------------------------------------------------------------------------------------
		ArrayList<String> martial_statusAttributsListe = (ArrayList<String>) marital_statusListe.clone();

		for (int i = 0; i < martial_statusAttributsListe.size(); i++) {
			if (martial_statusAttributsListe.get(i) != "done") {
				for (int j = 1; j < martial_statusAttributsListe.size(); j++) {
					if (martial_statusAttributsListe.get(i).equals(martial_statusAttributsListe.get(j)) && i != j) {
						martial_statusAttributsListe.set(j, "done");
					}
				}
			}
		}
		for (int i = 0; i < martial_statusAttributsListe.size(); i++) {
			if (martial_statusAttributsListe.get(i) != "done") {
				fmartial_statusAttributsListe.add(martial_statusAttributsListe.get(i));
				attributsListe.add("marital-status_" + martial_statusAttributsListe.get(i));
			}
		}
		// occupation
		// ------------------------------------------------------------------------------------------------------
		ArrayList<String> occupationAttributsListe = (ArrayList<String>) occupationListe.clone();

		for (int i = 0; i < occupationAttributsListe.size(); i++) {
			if (occupationAttributsListe.get(i) != "done") {
				for (int j = 1; j < occupationAttributsListe.size(); j++) {
					if (occupationAttributsListe.get(i).equals(occupationAttributsListe.get(j)) && i != j) {
						occupationAttributsListe.set(j, "done");
					}
				}
			}
		}
		for (int i = 0; i < occupationAttributsListe.size(); i++) {
			if (occupationAttributsListe.get(i) != "done") {
				foccupationAttributsListe.add(occupationAttributsListe.get(i));
				attributsListe.add("occupation_" + occupationAttributsListe.get(i));
			}
		}
		// relationship
		// ------------------------------------------------------------------------------------------------------
		ArrayList<String> relationshipAttributsListe = (ArrayList<String>) relationshipListe.clone();

		for (int i = 0; i < relationshipAttributsListe.size(); i++) {
			if (relationshipAttributsListe.get(i) != "done") {
				for (int j = 1; j < relationshipAttributsListe.size(); j++) {
					if (relationshipAttributsListe.get(i).equals(relationshipAttributsListe.get(j)) && i != j) {
						relationshipAttributsListe.set(j, "done");
					}
				}
			}
		}
		for (int i = 0; i < relationshipAttributsListe.size(); i++) {
			if (relationshipAttributsListe.get(i) != "done") {
				frelationshipAttributsListe.add(relationshipAttributsListe.get(i));
				attributsListe.add("relationship_" + relationshipAttributsListe.get(i));
			}
		}
		// race
		// ------------------------------------------------------------------------------------------------------
		ArrayList<String> raceAttributsListe = (ArrayList<String>) raceListe.clone();

		for (int i = 0; i < raceAttributsListe.size(); i++) {
			if (raceAttributsListe.get(i) != "done") {
				for (int j = 1; j < raceAttributsListe.size(); j++) {
					if (raceAttributsListe.get(i).equals(raceAttributsListe.get(j)) && i != j) {
						raceAttributsListe.set(j, "done");
					}
				}
			}
		}
		for (int i = 0; i < raceAttributsListe.size(); i++) {
			if (raceAttributsListe.get(i) != "done") {
				fraceAttributsListe.add(raceAttributsListe.get(i));
				attributsListe.add("race_" + raceAttributsListe.get(i));
			}
		}
		// native country
		// ------------------------------------------------------------------------------------------------------
		ArrayList<String> countryAttributsListe = (ArrayList<String>) countryListe.clone();

		for (int i = 0; i < countryAttributsListe.size(); i++) {
			if (countryAttributsListe.get(i) != "done") {
				for (int j = 1; j < countryAttributsListe.size(); j++) {
					if (countryAttributsListe.get(i).equals(countryAttributsListe.get(j)) && i != j) {
						countryAttributsListe.set(j, "done");
					}
				}
			}
		}
		for (int i = 0; i < countryAttributsListe.size(); i++) {
			if (countryAttributsListe.get(i) != "done") {
				fcountryAttributsListe.add(countryAttributsListe.get(i));
				attributsListe.add("country_" + countryAttributsListe.get(i));
			}
		}
		//income zu attributsliste hinzufuegen
		attributsListe.add("income");
		System.out.println("testtest");
	}

	public static void ausgabe() throws IOException {

		// Collections.sort(fPositionen);
		// fPositionen.sort(c);

		Workbook workbook = new XSSFWorkbook();
		OutputStream ExcelFileToRead = new FileOutputStream("Datensatz" + ".xlsx");
		// HSSFWorkbook workbook = new HSSFWorkbook();
		XSSFSheet sheet = (XSSFSheet) workbook.createSheet("Datensatz");
		Row row = sheet.createRow(0);
		Cell cell = row.createCell(0);
		// Attributszeile
		cell.setCellValue(attributsListe.get(0));
		for (int i = 1; i < attributsListe.size(); i++) {
			cell = row.createCell(i);
			cell.setCellValue(attributsListe.get(i));
		}
		// Wert Zeilen

		for (int i = 1; i < records.size(); i++) {
			int counter = 5;
			Row row2 = sheet.createRow(i);
			Cell cell2 = row2.createCell(0);

			cell2.setCellValue(fgenderListe.get(i - 1));
			cell2 = row2.createCell(1);
			cell2.setCellValue(fageListe.get(i - 1));
			cell2 = row2.createCell(2);
			cell2.setCellValue(feducationListe.get(i - 1));
			cell2 = row2.createCell(3);
			cell2.setCellValue(fcapital_gainsListe.get(i - 1));
			cell2 = row2.createCell(4);
			cell2.setCellValue(fcapital_lossListe.get(i - 1));
			// Ausgabe von one Hot encoding
			// one hot encoding: workclass, marital-status, occupation, relationship, race,
			// native-country
			// workclass
			for (int j = 0; j < fworkclassAttributsListe.size(); j++) {
				cell2 = row2.createCell(counter + j);
				if (workclassListe.get(i - 1).equals(fworkclassAttributsListe.get(j))) {
					cell2.setCellValue(1);
				} else {
					cell2.setCellValue(0);
				}
			}
			counter = counter + fworkclassAttributsListe.size();
			// martial.status
			for (int j = 0; j < fmartial_statusAttributsListe.size(); j++) {
				cell2 = row2.createCell(counter + j);
				if (marital_statusListe.get(i - 1).equals(fmartial_statusAttributsListe.get(j))) {
					cell2.setCellValue(1);
				} else {
					cell2.setCellValue(0);
				}
			}
			counter = counter + fmartial_statusAttributsListe.size();
			// occupation
			for (int j = 0; j < foccupationAttributsListe.size(); j++) {
				cell2 = row2.createCell(counter + j);
				if (occupationListe.get(i - 1).equals(foccupationAttributsListe.get(j))) {
					cell2.setCellValue(1);
				} else {
					cell2.setCellValue(0);
				}
			}
			counter = counter + foccupationAttributsListe.size();
			// relationship
			for (int j = 0; j < frelationshipAttributsListe.size(); j++) {
				cell2 = row2.createCell(counter + j);
				if (relationshipListe.get(i - 1).equals(frelationshipAttributsListe.get(j))) {
					cell2.setCellValue(1);
				} else {
					cell2.setCellValue(0);
				}
			}
			counter = counter + frelationshipAttributsListe.size();
			// race
			for (int j = 0; j < fraceAttributsListe.size(); j++) {
				cell2 = row2.createCell(counter + j);
				if (raceListe.get(i - 1).equals(fraceAttributsListe.get(j))) {
					cell2.setCellValue(1);
				} else {
					cell2.setCellValue(0);
				}
			}
			counter = counter + fraceAttributsListe.size();
			// country
			for (int j = 0; j < fcountryAttributsListe.size(); j++) {
				cell2 = row2.createCell(counter + j);
				if (countryListe.get(i - 1).equals(fcountryAttributsListe.get(j))) {
					cell2.setCellValue(1);
				} else {
					cell2.setCellValue(0);
				}
			}
			counter = counter + fcountryAttributsListe.size();
			//endausgabe >=50 k
			cell2 = row2.createCell(counter);
			cell2.setCellValue(incomeListe.get(i - 1));
		}
		
		
		
		
		System.out.println("End - Ausgabe");
		try {
			// workbook.close();
			workbook.write(ExcelFileToRead);

			File f = new File("Datensatz.xlsx");

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
