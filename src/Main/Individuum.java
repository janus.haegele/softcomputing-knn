package Main;

public class Individuum {

	int dim;
	double[] x;
	double signum;
	double fitness;
	public static double lernRate;
	public double neueFitness = 0;
	
	Individuum(int n) {
		this.dim = n;
		this.x = new double[n];
		for (int i = 0; i < n; i++) {
			x[i] = 5 * Math.random(); // problemspezifisch geraten
			if (Math.random() < 0.5)
				x[i] = -x[i];
		}

		this.signum = Math.random();
		lernRate = 1.0 / Math.sqrt((double) (n)); // Schwefel95
		// lernRate = 0.5; // probieren

	}
	public void setFitness(double fitness) {
		System.out.println("Fitnessmethode Indi" + fitness);
		neueFitness = fitness;
	}
	public void fitness() {
		// Rastigrin-Funktion
		// Minimierung
		// Minimum = 0.0 bei x[i] = 0
		
		
//		double fit = 10.0 * dim;
//		for (int i = 0; i < x.length; i++) {
//			fit += (Math.pow(x[i], 2) - 10 * Math.cos(2 * Math.PI * x[i]));
//		}
//		this.fitness = fit;
		this.fitness = neueFitness;
	}

	public void mutieren() {//signum = schrittweite
		double zz = lernRate * snv();//zz = zufallszahl 
		this.signum = this.signum * Math.exp(zz);
		
		if (signum < 0)
			zz /= 0;
		
		for (int i = 0; i < this.x.length; i++) {
			zz = this.signum * snv();
			this.x[i] = this.x[i] + zz;
		}
	}

	public void rekombinieren(Individuum e1, Individuum e2) {
		this.signum = (e1.signum + e2.signum) / 2.;
		for (int i = 0; i < this.x.length; i++) {
			double zz = Math.random();
			if (zz < 0.5)
				this.x[i] = e1.x[i];
			else
				this.x[i] = e2.x[i];
		}
	}

	//private static double snv() {#
	public double snv() {
		// Methode von BoxMuller
		double z1 = Math.random();
		double z2 = Math.random();
		double x1 = Math.cos(z1 * 2 * Math.PI) * Math.sqrt(-2 * Math.log(z2));
		double x2 = Math.sin(z1 * 2 * Math.PI) * Math.sqrt(-2 * Math.log(z2));
		if (Math.random() < 0.5)
			return x1;
		else
			return x2;
	}

}
