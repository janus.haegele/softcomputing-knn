package Main;
public class ES {

	private int dimension;
	private int mue;
	private int lam;
	private int maxIter;
	private boolean elitaer;
	private boolean maximierung;

	private Individuum[] POP;
	private Individuum[] CHILD;

	
	public double neueFitness = 0;
	public static void main(String args[]) {
		//ES eineES = new ES(20, 200, 10, 100, true, false);
		ES eineES = new ES(2, 4, 8, 1000, false, false);
		eineES.run();
		
	}

	public ES(int dimension, int mue, int lam, int maxIter, boolean elitaer, boolean maximierung) {

		this.dimension = dimension;
		this.mue = mue;
		this.lam = lam;
		this.maxIter = maxIter;
		this.elitaer = elitaer;
		this.maximierung = maximierung;

		POP = new Individuum[mue];
		CHILD = new Individuum[lam];

		for (int i = 0; i < mue; i++) {
			Individuum indi = new Individuum(dimension);
			indi.fitness();
			// System.out.println(i + " elter-> " + indi.fitness);
			POP[i] = indi;
		}
		if (elitaer)
			sort(POP);
	}
	public void setFitness(double fitness1) {
		System.out.println("Fitnessmethode Es" + fitness1);
		neueFitness = fitness1;
	}
	public double run() {
		double ergebnis = 0;
		for (int i = 0; i < maxIter; i++) {

			System.out.println(
					i + " -> " + POP[0].fitness + " " + POP[0].x[0] + " " + POP[0].signum + " " + Individuum.lernRate);

			for (int k = 0; k < lam; k++) {
				int p1 = (int) (POP.length * Math.random());
				int p2 = (int) (POP.length * Math.random());
				Individuum indi = new Individuum(dimension);

				indi.rekombinieren(POP[p1], POP[p2]);
				indi.mutieren();
				
				indi.setFitness(neueFitness);
				indi.fitness();
				
				CHILD[k] = indi;
				ergebnis = indi.signum;
			}

			if (elitaer)
				replacementElitaer();
			else
				replacement();
		}
		return ergebnis;
		
	}

	private void replacement() {
		sort(CHILD);
		for (int i = 0; i < mue; i++) {
			POP[i] = CHILD[i];
		}
	}

	private void replacementElitaer() {
		sort(CHILD);
		Individuum[] popNeu = new Individuum[mue];
		int i = 0;
		int j = 0;
		for (int k = 0; k < mue; k++) {
			if (maximierung) {
				if (j < CHILD.length && CHILD[j].fitness >= POP[i].fitness) {// maximierung
					popNeu[k] = CHILD[j];
					j++;
				} else {
					popNeu[k] = POP[i];
					i++;
				}
			} else {
				if (j < CHILD.length && CHILD[j].fitness <= POP[i].fitness) {// maximierung mini?
					popNeu[k] = CHILD[j];
					j++;
				} else {
					popNeu[k] = POP[i];
					i++;
				}
			}

		}
		POP = popNeu;
	}

	private void sort(Individuum[] liste) {
		mergesort(liste, 0, liste.length - 1);
	}

	private void mergesort(Individuum[] s, int l, int r) {
		if (l < r) {
			int m = (l + r + 1) / 2;
			mergesort(s, l, m - 1);
			mergesort(s, m, r);
			merge(s, l, m, r);
		}
	}

	private void merge(Individuum[] s, int li, int mi, int re) {
		Individuum[] temp = new Individuum[re - li + 1];
		for (int i = 0, j = li, k = mi; i < temp.length; i++) {
			if (!maximierung) {
				if ((k > re) || ((j < mi) && (s[j].fitness < s[k].fitness))) {
					temp[i] = s[j];
					j++;
				} else {
					temp[i] = s[k];
					k++;
				}
			} else {
				if ((k > re) || ((j < mi) && (s[j].fitness > s[k].fitness))) {
					temp[i] = s[j];
					j++;
				} else {
					temp[i] = s[k];
					k++;
				}
			}
		}
		for (int n = 0; n < temp.length; n++) {
			s[li + n] = temp[n];
		}
	}

}
