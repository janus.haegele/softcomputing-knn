package Main;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.category.LineRenderer3D;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 * This program demonstrates how to draw line chart with CategoryDataset using
 * JFreechart library.
 * 
 * @author www.codejava.net
 *
 */

@SuppressWarnings("serial")
public class DrawGraph extends JFrame {
	
	private DefaultCategoryDataset dataset = new DefaultCategoryDataset();
	
	private String series1 = "Optimierungs Kurve";
	
	
	public DrawGraph(ArrayList<Integer> liste) {
		super("Line Chart Example with JFreechart");
		//start();
		updateDataSet(liste);
		init();
		
	}
	public void init() {
		
		JPanel chartPanel = createChartPanel();
		add(chartPanel, BorderLayout.CENTER);

		setSize(2500, 1200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}
	private JPanel createChartPanel() {
		String chartTitle = "Fehleranzahl pro Epoche";
		String categoryAxisLabel = "Epochen";
		String valueAxisLabel = "Fehler Anzahl";

		//CategoryDataset dataset = createDataset();

		
		JFreeChart chart = ChartFactory.createLineChart(chartTitle, categoryAxisLabel, valueAxisLabel, dataset);

		
		
//		boolean showLegend = false;
//		boolean createURL = false;
//		boolean createTooltip = false;
//		
//		JFreeChart chart = ChartFactory.createLineChart(chartTitle, 
//				categoryAxisLabel, valueAxisLabel, dataset, 
//				PlotOrientation.HORIZONTAL, showLegend, createTooltip, createURL);

//		JFreeChart chart = ChartFactory.createLineChart3D(chartTitle, 
//				categoryAxisLabel, valueAxisLabel, dataset);		
		
		customizeChart(chart);

		// saves the chart as an image files
		
		File imageFile = new File("LineChart.png");
		int width = 640;
		int height = 480;

		try {
			ChartUtilities.saveChartAsPNG(imageFile, chart, width, height);
		} catch (IOException ex) {
			System.err.println(ex);
		}
		 
		
		return new ChartPanel(chart);
	}
	
	public void updateDataSet(ArrayList<Integer> liste) {
		for (int i = 0; i < liste.size(); i++) {
			dataset.addValue(liste.get(i), series1, String.valueOf(i+1));
		}
	}


	private void customizeChart(JFreeChart chart) {
		CategoryPlot plot = chart.getCategoryPlot();
		LineAndShapeRenderer renderer = new LineAndShapeRenderer();

		// sets paint color for each series
		renderer.setSeriesPaint(0, Color.RED);

		// sets thickness for series (using strokes)
		renderer.setSeriesStroke(0, new BasicStroke(1.2f));

		// sets paint color for plot outlines
		plot.setOutlinePaint(Color.BLUE);
		plot.setOutlineStroke(new BasicStroke(1.2f));

		// sets renderer for lines
		plot.setRenderer(renderer);

		// sets plot background
		plot.setBackgroundPaint(Color.CYAN);

		// sets paint color for the grid lines
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.BLACK);

		//plot.setDomainGridlinesVisible(true);
		//plot.setDomainGridlinePaint(Color.BLACK);

	}
	public void start() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				//new DrawGraph(null).setVisible(true);
			}
		});
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				//new DrawGraph().setVisible(true);
			}
		});
	}
}