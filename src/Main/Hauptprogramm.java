package Main;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Hauptprogramm {
	/*
	 * TO DOs 2x2 Ausgabemartix runden auf ganze zahlen f�r crossvalidation
	 * 
	 */
	public static void shuffle(double[][] a) {
		Random random = new Random();

		for (int i = a.length - 1; i > 0; i--) {
			for (int j = a[i].length - 1; j > 0; j--) {
				int m = random.nextInt(i + 1);
				// int n = random.nextInt(j + 1);

				double[] temp = a[i];
				a[i] = a[m];
				a[m] = temp;
			}
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		// Test der shuffle Methode Fisher-Yates shuffle
		// ---------------------------------------------
		/*
		 * double [][] test = new double[10][5]; int counter = 0; for (int i = 0; i <
		 * 10; i++) { for (int j = 0; j < 5; j++) { test[i][j] = counter; counter++; } }
		 * shuffle(test); for (int i = 0; i < test.length; i++) { for (int j = 0; j <
		 * test[i].length; j++) { System.out.print(test[i][j] + " "); }
		 * System.out.println(); }
		 */
		// ---------------------------------------------

//		double[][] daten = Einlesen.einlesenBankdaten(new File("4_Trainingsdaten.csv"));
//		double[][] daten = Einlesen.einlesenDiabetes(new File("diabetes.csv"), true);
//		double[][] daten = Einlesen.einlesenVorlesungsbeispiele(new File("svmKnearestNick.txt"));

//		double[][] daten = Einlesen.einlesenVorlesungsbeispiele(new File("XOR.txt"));

//		double[][] daten = Einlesen.einlesenVorlesungsbeispiele(new File("wetter.txt"));

//		double[][] daten = Einlesen.einlesenCensusdaten(new File("Datensatz.csv"));

		// Daten Einlesen
		
//		double[][] daten = Einlesen.einlesenDiabetes(new File("diabetes.csv"), true);
		double[][] daten = Einlesen.einlesenBankdaten(new File("Datensatz.csv"));
		shuffle(daten); // Datens�tze zuf�llig anordnen

		// Crossvalidation kooeffizient
		int cvk = 3;
		double[][] cvListe = new double[cvk][12];

		int anzDaten = daten.length / cvk;
		System.out.println(daten.length);
		System.out.println(anzDaten);

		double[][] testdaten = new double[anzDaten][];
		// traindaten calculator
		int verlust = daten.length - (anzDaten * (cvk));
		System.out.println("Verlust" + verlust);
		int tdata = (anzDaten * (cvk - 1)) + verlust;
		System.out.println(tdata);
		System.out.println(anzDaten + tdata);
		double[][] traindaten = new double[tdata][];
		// Crossvalidation mehrmals ausf�hren
		for (int i = 0; i < cvk; i++) {

			// int anzDaten = daten.length/3;
			for (int j = 0; j < anzDaten; j++) {
				testdaten[j] = daten[j];
			}
			for (int j = 0; j < tdata; j++) {
				traindaten[j] = daten[j];
			}

			int dimension = 0;
			dimension = daten[0].length - 1;

			int[] strukturNN = { 5 };// anzahl Knoten (incl. Bias) pro Hiddenschicht
			KNN netz = new KNN(dimension, strukturNN);

			netz.trainieren(traindaten);// Verlustfunktion min
//			netz.trainierenStochastisch(traindaten);

			cvListe[i] = netz.evaluieren(testdaten);

		}
		// Augabe Crossvalidation
		evaluationsAusgabe(cvListe, cvk);
		// Einlesen.auslesen(daten);
		// netz.trainierenStochastisch(daten);
		// netz.trainierenMiniBatch(daten);
		// netz.trainierenBatch(daten);

//		daten = Einlesen.einlesenBankdaten(new File("test01.csv")); 	
//		daten = Einlesen.einlesenBankdaten(new File("5_Testdaten.csv")); 	
//		daten = Einlesen.einlesenDiabetes(new File("diabetes.csv"), false);
		// Einlesen.auslesen(daten);
//		netz.evaluieren(daten);

		// netz.evaluierenGUIII(daten);

	}

	// Crossvalidation alle ergebnisse zusammenzaehlen und durch koefizient teilen
	public static void evaluationsAusgabe(double[][] testdaten, int cvk) {
		double[] ergebnis = new double[12];
		for (int i = 0; i < testdaten.length; i++) {
			for (int j = 0; j < 12; j++) {

				ergebnis[j] = (double) ergebnis[j] + (double) testdaten[i][j];

			}
		}
		for (int i = 0; i < ergebnis.length; i++) {
			ergebnis[i] = ergebnis[i] / cvk;
		}
		System.out.println();
		System.out.println("Crossvalitation Finale Ausgabe");
		System.out.println("-----------------------------------");
		System.out.println("Anzahl Muster:  \t" + ergebnis[0]);
		System.out.println("Anzahl Positiv: \t" + ergebnis[1]);
		System.out.println("Anzahl Negativ: \t" + ergebnis[2]);
		System.out.println("Anteil Positiv: \t" + ergebnis[3]);
		System.out.println("Anteil Negativ: \t" + ergebnis[4]);

		System.out.println("Genauigkeit  :  \t" + ergebnis[5]);
		System.out.println("Trefferquote:   \t" + ergebnis[10]);
		System.out.println("Ausfallrate :   \t" + ergebnis[11]);

		System.out.println("richtigPositiv: \t" + ergebnis[6]);
		System.out.println("falsch Positiv: \t" + ergebnis[7]);

		System.out.println("richtigNegativ: \t" + ergebnis[8]);
		System.out.println("falsch Negativ: \t" + ergebnis[9]);
		
		System.out.println("richtigPositiv Rate: \t" + round(ergebnis[6]/ergebnis[1]*100, 2) + "%");
		System.out.println("richtigNegativ Rate: \t" + round(ergebnis[8]/ergebnis[2]*100, 2) + "%");
		System.out.println("-----------------------------------");
	}
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = BigDecimal.valueOf(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
}
